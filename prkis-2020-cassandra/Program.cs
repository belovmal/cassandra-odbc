﻿using System;
using System.Data;
using System.Data.Odbc;

namespace prkis_2020_cassandra
{
    class Program
    {
        static void Main(string[] args)
        {

            // 1. Create Connection String (add DSN, add Keyspace (database)

            OdbcConnectionStringBuilder casconparam = new OdbcConnectionStringBuilder();
            casconparam.Add("Dsn", "prkis");
            casconparam.Add("Database", "usermanage");

            // 2. Create Connection to PRKIS Cassandra cluster
            OdbcConnection con = new OdbcConnection(casconparam.ConnectionString);
            con.Open();

            // 3. Create SQL statement and Command (ExecuteQuery() [list of rows ex. select *...], 
            //  ++ ExecuteScalar() [single value, use aggregation functions count, max, min, mean...],
            //   ExecuteNonQuery [run INSERT, UPDATE, DELETE, TRUNCATE statements]

            string SQL1 = "select count(*) from accounts where login=@login and pwd=@pwd";

            OdbcCommand cmd = new OdbcCommand(SQL1, con);

            // 3.1 Send parameteres to SQL statement safely

            OdbcParameter p_login = new OdbcParameter
            {
                ParameterName = "@login",
                DbType = DbType.String,
                Value = "a"
            };
            cmd.Parameters.Add(p_login);

            OdbcParameter p_pwd = new OdbcParameter
            {
                ParameterName = "@pwd",
                DbType = DbType.String,
                Value = "b"
            };
            cmd.Parameters.Add(p_pwd);

            // Cast Object Abstract Type to Integer becase stament can return only 0 or 1
            int result = Convert.ToInt32(cmd.ExecuteScalar());

            if (result == 1)
            {
                Console.WriteLine("Auth PASS...");
            }
            else
            {
                Console.WriteLine("Auth FAIL...");
            }

            // Show Connection State
            // Console.WriteLine("Connection state {0} ", con.State);

            // 3. Dispose Command and Close Connection
            cmd.Dispose();
            // ===========
            con.Close();
            con.Dispose();
        }
    }
}
